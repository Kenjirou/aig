$(document).ready(function() {
  // Script for description tabs
  $('ul.tabs li').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });

  // Scripts for showing phones in contacts and menu item
  $("#show_tel").click(function() {
    $("#hidden_tel").removeClass('hidden');
  });

  $("#click_phone").click(function() {
    $("#hidden_tel").addClass('hidden');
  });

  $("#units").click(function() {
    $(".hdn_menu").removeClass('hidden');
  });

  $("#hidden_menu").click(function() {
    $(".hdn_menu").addClass('hidden');
  });
})

  // Scripts for the form
  $("#shw_form").click(function() {
    $("#cont_form").removeClass('hidden');
  });
